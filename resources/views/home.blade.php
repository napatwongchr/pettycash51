@extends('app')

@section('content')
    <div class="row">
            <div class="col-lg-12">
                    <div class="panel panel-default">
                            <div class="panel-heading">Main Dash</div>

                            <div class="panel-body">
                                    Main Dashboard
                            </div>
                    </div>
            </div>
    </div>
    <div class="row">
            <div class="col-lg-6">
                    <div class="panel panel-default">
                            <div class="panel-heading">Panel 1</div>

                            <div class="panel-body">
                                    panel 1
                            </div>
                    </div>
            </div>
            <div class="col-lg-6">
                    <div class="panel panel-default">
                            <div class="panel-heading">Panel 2</div>

                            <div class="panel-body">
                                    panel 2
                            </div>
                    </div>
            </div>
    </div>
@endsection