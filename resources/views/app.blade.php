<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sawasdee Petty Cash</title>

        <!-- Css -->
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/mycascading.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/simple-sidebar.css') }}" rel="stylesheet">

    {{--picadate css--}}
        <link href="{{ asset('/css/classic.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/classic.date.css') }}" rel="stylesheet">

    {{--jquery confirm css--}}
    <link href="{{ asset('/css/jquery-confirm.min.css') }}" rel="stylesheet">
        
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default mynav">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Sawasdee Petty Cash</a>
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="{{ url('/home') }}">Dashboard <span class="sr-only">(current)</span></a></li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actions <span class="caret"></span></a>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{url('/financial')}}">Add Financial Amount</a></li>
                                        <li><a href="{{ url('/expense') }}">Add Expense</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('/category') }}">Add Expense Category</a></li>
									   <li><a href="{{ url('/userExpense') }}">User Expense</a></li>
                                      </ul>
                                    </li>
                                </ul>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Scripts -->
	{{--<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>--}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('/js/jquery-confirm.min.js')}}"></script>
    <script src="{{asset('/js/picker.js')}}"></script>
    <script src="{{asset('/js/picker.date.js')}}"></script>
    <script src="{{asset('/js/date.js')}}"></script>


	@yield('custom_js')

</body>
</html>
