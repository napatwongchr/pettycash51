@extends('app')

@section('content')


    <div class="add-expense">
        <h2>All Expenses of date </h2>
        <a href="{{url('/expense/create')}}">create</a>

    </div>

    <div id="startdateShow" class="form-group col-lg-6">
        {!! Form::label('lbStartDate','Start Date') !!}
        {{--{!! Form::text('startDate', null, ['class' => 'form-control datepicker','id' => 'startDate',--}}
        {{--'readonly']) !!}--}}
        <div class="controls">
            <div class="input-group">
                <input name="startDateShow" readonly id="startDateShow" type="text" class="date-picker form-control toyotadate" />

                <label for="startDateShow" class="input-group-addon btn">
                    <span class="glyphicon glyphicon-calendar"></span>
                </label>
            </div>
            <input name="startDate" type="hidden" id="startDate">
        </div>
    </div>

    <div id="enddateShow" class="form-group col-lg-6">
        {!! Form::label('lbEndDate','End Date') !!}
        {{--{!! Form::text('endDate', null, ['class' => 'form-control datepicker','id' => 'endDate']) !!}--}}

        <div class="controls">
            <div class="input-group">
                <input name="endDateShow" readonly id="endDateShow" type="text" class="date-picker form-control toyotadate" />

                <label for="endDateShow" class="input-group-addon btn">
                    <span class="glyphicon glyphicon-calendar"></span>
                </label>
            </div>
            <input name="endDate" type="hidden" id="endDate">
        </div>

    </div>

    <div class="row">
        <button id="ajaxSubmit" class="form-control"> Ajax query</button>
    </div>

    {!! Form::open([ 'method'   =>  'POST',
                    'name'      =>  'submitStatus' ,
                    'action'   =>  'FinancialController@storeStatus'])!!}
    <div class="row" id="queryTable">

    </div>
    <div class="form-group">
        <input class="form-control" type="submit">
    </div>
    {!! Form::close() !!}
@stop

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/financial_js.js')}}"></script>
@stop
