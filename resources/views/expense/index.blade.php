@extends('app')

@section('content')
    <div class="add-expense">
        <h2>Expenses</h2>
        <a href="{{url('/expense/create')}}">create</a>
    </div>

    <div class="row">
        <table class="table table-striped">
            <thead>
            <th>Bill id</th>
            <th>pay Date</th>
            <th>user Name</th>
            <th> total cost</th>
            <th> status</th>
            <th> approve Date</th>
            <th> edit</th>
            <th> delete</th>
            </thead>
            <tbody>
            @foreach($expenses as $expense)
            <tr>
                <td>{{$expense->id}}</td>
                <td>{{$expense->buyDate}}</td>
                <td>{{$users->where('id',$expense->user_id)->value('name')}}</td>
                <td>{{$expense->totalCost}}</td>
                <td>{{$expense->status}}</td>
                <td>{{$expense->approveDate}}</td>
                <td> <a href="expense/{{$expense->id}}/edit" class="btn btn-primary" role="button">edit</a></td>
                <td> {!! Form::open(['method'    =>  'DELETE',
                                                        'name'      =>  'id' . $expense->id,
                                                        'action'   =>  ['ExpenseController@destroy',$expense->id]])!!}

                    <button type="submit" class="btn btn-primary">Delete
                    </button>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/expense_js.js')}}"></script>
@stop