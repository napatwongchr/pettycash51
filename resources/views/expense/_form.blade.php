<!-- Date Row -->
<div class="row">
    <div id="buydate" class="form-group col-lg-6">
        {!! Form::label('lbBoughtDate','Bought Date') !!}
        <div class="input-group">
            <input class="form-control dateReadonly" type="text" id="input_date" name="input_date">
            <label for="input_date" class="input-group-addon btn">
                <span class="glyphicon glyphicon-calendar"></span>
            </label>
        </div>

        @if($expense != null)
            <input name="boughtDate" type="hidden" id="boughtDate" value="{{$expense->buyDate}}">
        @else
            <input name="boughtDate" type="hidden" id="boughtDate">
        @endif

<!--        <div class="controls">
            <div class="input-group">
                <input name="buydate" readonly id="buyDate" type="text" class="date-picker form-control" />
                <label for="lbbuydate" class="input-group-addon btn">
                    <span class="glyphicon glyphicon-calendar"></span>
                </label>
            </div>
        </div>-->

    </div>
</div>

<div class="row">
    <div id="startdate" class="form-group col-lg-12">
        <h4>Expense Items</h4>
        <table class="table table-striped" id="expenseTable">
            <thead>
                <th>#</th>
                <th>Item Type</th>
                <th>Description</th>
                <th>Remark</th>
                <th>Cost</th>
                <th>Create Date</th>
                <th>Actions</th>
            </thead>
            <tbody class="expenseTableBody" id="expenseItem">
            {{--*/ $i = 1 /*--}}
            @forelse($arrayOfLineItem as $lineItem)
                <tr>
                    <td>{{$i}}</td>
                    <td class="item-type">
                        <select class="form-control"  name="itemType[]">
                            @foreach($arrayOfItemType as $itemType)
                                <option value="{{$itemType->id}}" {{ $lineItem->itemType_id == $itemType->id? 'selected' :'' }} >{{$itemType->name}}</option>
                                @endforeach
                        </select>
                    <td class="item-desc">
                        <input class="hidden"  name="description[]" type="text" value="{{$lineItem->description}}">
                        <span class="descval" style="color: red;">{{$lineItem->description}}</span>
                    </td>
                    <td class="item-remark">
                        <input class="hidden"  name="remark[]" type="text" value="{{$lineItem->remark}}">
                        <span class="remarkval" style="color: red;">{{$lineItem->remark}}</span>

                        </td>
                    <td class="item-cost">
                        <input class="hidden"  name="cost[]" type="text" value="{{$lineItem->cost}}">
                        <span class="costval" style="color: red;">{{$lineItem->cost}}</span>
                        </td>
                    <td class="item-createDate">
                        <input class="hidden" name="createDate[]" value="{{$lineItem->crateDate}}">
                        <span class="createDateVal" style="color: red;">{{$lineItem->createDate}}</span>
                    </td>
                    <td class="item-actions">
                        {{--edit,delete--}}
                        <button type="button" class="btn btn-default btn-sm edit-row-btn">Edit</button>
                        <button type="button" class="btn btn-default btn-sm delete-row-btn">Delete</button>
                    </td>
                {{--*/ $i++ /*--}}
                </tr>
            @empty
                {{--do nothing--}}
            @endforelse

                <tr>
                    <td>{{$i}}</td>
                    <td class="item-type">
                        <select class="form-control" name="itemType[]">
                            @foreach($arrayOfItemType as $itemType)
                                <option value="{{$itemType->id}}">{{$itemType->name}}</option>
                            @endforeach
                        </select></td>
                    <td class="item-desc"><input class="form-control" id="description" name="description_input" type="text"></td>
                    <td class="item-remark"><input class="form-control" id="remark" name="remark_input" type="text"></td>
                    <td class="item-cost"><input class="form-control" id="cost" name="cost_input" type="text"></td>
                    <td class="item-createDate">
                        <input class="form-control" id="createDate" name="createDate_input" type="text" value="{{date('Y-m-d')}}" readonly></td>
                    <td class="item-actions">
                        <button type="button" class="btn btn-default btn-sm add-row-btn">Add</button>
                    </td>
                </tr>


            </tbody>
        </table>


        <div id="typeList" class="hidden">
            <select class="form-control"  name="itemType[]">
                @foreach($arrayOfItemType as $itemType)
                    <option value="{{$itemType->id}}" >{{$itemType->name}}</option>
                @endforeach
            </select>
        </div>


        {{--<button id="addNewItem" type="button" class="btn btn-default btn-lg add-newrow-btn center-block">--}}
            {{--<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Item--}}
        {{--</button>--}}
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-8">

    </div>
    <div class="form-group col-lg-4">
        {!! Form::label('lbTotalCost','Total Cost') !!}
        @if($expense != null)
        <input id="totalCost" name="totalCost" class="form-control" placeholder="Total Cost" readonly value="{{$expense->totalCost}}">
        @else
        <input id="totalCost" name="totalCost" class="form-control" placeholder="Total Cost" readonly>
        @endif
    </div>

</div>

<div class="hidden">
    <input id="thisDate" value="{{date('Y-m-d')}}">
</div>

