{{--*/ $menu = 'Event' /*--}}
@extends('app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid fontbody">
            <div class="form-group">
                {!! Form::open(array(
                                'method' => 'PATCH',
                                'action' => ['ExpenseController@update', $expense->id],
                                'class' => 'form')) !!}
                <div class="page-header">
                    <h1 class="h1topic">Expense Details</h1>
                    {!! Form::submit('Update Expense', ['class' => 'submit-btn btn btn-success-sawasdee pull-right submitbtn']) !!}
                </div>

                @include('expense._form')
                {!!Form::close()!!}

            </div>
        </div>
    </div>
@stop

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/expense_js.js')}}"></script>
@stop
