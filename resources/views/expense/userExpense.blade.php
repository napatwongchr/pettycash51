@extends('app')

@section('content')
    <div class="add-expense">
        <h2>Expenses Of {{$userName}}</h2>
        <a href="{{url('/expense/create')}}">create</a>
    </div>

    <div class="row">
        <table class="table table-striped">
            <thead>
            <th>buy Date</th>
            <th> created time</th>
            <th>description</th>
            <th> cost</th>
            <th>status</th>
            <th>Approve Date</th>

            <th> edit</th>
            <th> delete</th>
            </thead>
            <tbody>
            @foreach($expenseItems as $expenseItem)
                <tr>
                    <td>{{$expenseItem->buyDate}}</td>
                    <td>{{$expenseItem->createDate}}</td>
                    <td>{{$expenseItem->description}}</td>
                    <td>{{$expenseItem->cost}}</td>
                    <td>{{$expenseItem->status}}</td>
                    <td>{{$expenseItem->approveDate}}</td>
                    <td>
                        {!! Form::open([ 'method'   =>  'GET',
                           'name'      =>  'submitStatus' ,
                           'action'   =>  ['ExpenseController@edit',$expenseItem->expenseID]])!!}
                        <button class="btn btn-primary"> Edit</button>
                        {!! Form::close() !!}
                    <td>
                        {!! Form::open([ 'method'   =>  'POST',
                            'name'      =>  'submitStatus' ,
                            'action'   =>  'ExpenseController@delete'])!!}
                        <input name="itemID" class="hidden" value="{{$expenseItem->itemID}}">
                        <button class="btn btn-danger" onclick="return confirm()"> Delete</button>
                        {!! Form::close() !!}
                        {{--{!! Form::open(['method'    =>  'DELETE',--}}
                                                        {{--'name'      =>  'id' . $expense->id,--}}
                                                        {{--'action'   =>  ['ExpenseController@destroy',$expense->id]])!!}--}}

                        {{--<button type="submit" class="btn btn-primary">Delete</button>--}}
                        {{--{!! Form::close() !!}--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
