@extends('app')

@section('content')
    <div class="add-expense">
        <h2>Category</h2>
        <a href="{{url('/category/create')}}">create</a>
    </div>

    <div class="row">
        <table class="table table-striped">
            <thead>
            <th>id</th>
            <th>Name</th>
            <th> edit</th>
            <th> delete</th>
            </thead>
            <tbody>
            @foreach($arrayOfItemType as $itemType)
                <tr>
                    <td>{{$itemType->id}}</td>
                    <td>{{$itemType->name}}</td>
                    <td> <a href="category/{{$itemType->id}}/edit" class="btn btn-primary" role="button"> edit</a> </td>
                    <td> {!! Form::open(['method'    =>  'DELETE',
                                                        'name'      =>  'id' . $itemType->id,
                                                        'action'   =>  ['CategoryController@destroy',$itemType->id]])!!}

                        <button type="submit" class="btn btn-primary">Delete
                        </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/expense_js.js')}}"></script>
@stop