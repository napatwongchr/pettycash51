{{--*/ $menu = 'Event' /*--}}
@extends('app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid fontbody">
            <div class="form-group">
                {!! Form::model($itemType, array('route' => array('category.update',$itemType->id),'method'  =>  'PUT')) !!}
                <div class="page-header">
                    <h1 class="h1topic">Category Details</h1>
                    {!! Form::submit('Update Category', ['class' => 'submit-btn btn btn-success-sawasdee pull-right submitbtn']) !!}
                </div>

                @include('category._form')
                {!!Form::close()!!}

            </div>
        </div>
    </div>
@stop

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/expense_js.js')}}"></script>
@stop
