{{--*/ $menu = 'Event' /*--}}
@extends('app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid fontbody">
            <div class="form-group">
                {!! Form::open(array(
                                'method' => 'post',
                                'url' => 'category',
                                'class' => 'form')) !!}
                <div class="page-header">
                    <h1 class="h1topic">Category Details</h1>
                    {!! Form::submit('Add Expense', ['class' => 'submit-btn btn btn-success-sawasdee pull-right submitbtn']) !!}
                </div>

                @include('category._form')
                {!!Form::close()!!}

            </div>
        </div>
    </div>
@stop

@section('custom_js')
        <!-- custom js-->
    <script src="{{asset('/js/expense_js.js')}}"></script>
@stop