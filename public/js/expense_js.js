/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(document).ready(function($) {

    /*** On page load perform these options***/
    console.log( "ready!" );

    //var itemTypeDDListHidden = $('select.hidden.test');
    var itemTypeDDList = $('#typeList').html();

    bindEditRowBtn();
    bindDeleteBtn();
    bindAddToRowBtn();

    //checkRowLength();
    //numberOnly();

    /*** Events ***/

    /* PickAdate */
    var buyDate = $('#boughtDate').val();
    var $input = $('#input_date').pickadate();
    var picker = $input.pickadate('picker');

    //var bdate = new Date(buydate);
    //console.log(bdate);
    if(buyDate != "")
    {
        picker.set('select', buyDate ,{format: 'yyyy/mm/dd'});
        //picker.set('min',new Date(buydate));
    }
    else //set date to now
    {
        picker.set('select', new Date(),{format: 'yyyy/mm/dd'});
        $('#boughtDate').val(picker.get('select','yyyy/mm/dd'));
    }
    $('#input_date').on(
        {
            change: function()
            {
                $('#boughtDate').val(picker.get('select','yyyy/mm/dd'));
                //picker.set('min',new Date($('#boughtdate').val()));
            }
        });


    /* Add new row */
    $(".add-newrow-btn").click(function(){

        addNewRow();
        unBindDeleteBtn();
        bindDeleteBtn();
        unBindAddToRowBtn();
        bindAddToRowBtn();
        //numberOnly();

    });


    function bindNewRow()
    {
        addNewRow();
        unBindDeleteBtn();
        bindDeleteBtn();
        unBindAddToRowBtn();
        bindAddToRowBtn();
    }

    /* Delete row */

    /*** Functions ***/
    function addItemInRow(element){
        var itemTypeElement = $(element).closest("tr").children("td.item-type");
        var itemDescElement = $(element).closest("tr").children("td.item-desc");
        var itemRemarkElement = $(element).closest("tr").children("td.item-remark");
        var itemCostElement = $(element).closest("tr").children("td.item-cost");
        var itemCreateDateElement = $(element).closest("tr").children("td.item-createDate");
        var itemActionElement = $(element).closest("tr").children("td.item-actions").children();
        //var editBtn = $('<button type="button" class="btn btn-default btn-sm edit-row-btn">Edit</button>');
        var editBtn = '<button type="button" class="btn btn-default btn-sm edit-row-btn">Edit</button>' +
            '<button type="button" class="btn btn-default btn-sm delete-row-btn">Delete</button>';


        var typeVal = itemTypeElement.children().val();
        var descVal = itemDescElement.children().val();
        var remarkVal = itemRemarkElement.children().val();
        var createDateVal = itemCreateDateElement.children().val();
        var costVal = itemCostElement.children().val();

        //hide input field
        itemDescElement.children().remove();
        itemRemarkElement.children().remove();
        itemCreateDateElement.children().remove();
        itemCostElement.children().remove();



        itemDescElement.append('<span class="descval" style="color: red;">'+descVal+'</span>' +
            '<input name="description[]" class="hidden" value="'+descVal+'">');
        itemRemarkElement.append('<span class="remarkval" style="color: red;">'+remarkVal+'</span>' +
            '<input name="remark[]" class="hidden" value="'+remarkVal+'">');
        itemCostElement.append('<span class="costval" style="color: red;">'+costVal+'</span>' +
            '<input name="cost[]" class="hidden" value="'+costVal+'">');
        itemCreateDateElement.append('<span class="createDateVal" style="color: red;">'+createDateVal+'</span>' +
            '<input name="createDate[]" class="hidden" value="'+createDateVal+'" readonly>');


        var actionElementParent = itemActionElement.parent();
        $(element).remove(); //remove add button
        console.log(actionElementParent);
        actionElementParent.append(editBtn);

        //console.log($('#expenseItem tr').eq(0));
        totalExpense();

        bindNewRow();


        unBindEditRowBtn();
        bindEditRowBtn();


    }

    function totalExpense()
    {
        var length = $('#expenseItem tr').length;
        var total = 0;

        for(var i=0; i<length ;i++)
        {
            var thisCost = parseFloat($('#expenseItem tr').eq(i).find(".item-cost .costval").html());
            //var thisCost = $('#expenseItem tr').eq(i).find(".item-cost .costval").html();
            if(!isNaN(thisCost))
            {
                console.log('nan not in');
                total += thisCost;
            }
            console.log(total);
            $('#totalCost').val(total);
        }
    }

    function editItemInRow(element)
    {
        console.log('edit row ok');
        var thisDate = $('#thisDate').val();

        var itemTypeElement = $(element).closest("tr").children("td.item-type");
        var itemDescElement = $(element).closest("tr").children("td.item-desc");
        var itemRemarkElement = $(element).closest("tr").children("td.item-remark");
        var itemCostElement = $(element).closest("tr").children("td.item-cost");
        var itemCreateDateElement = $(element).closest("tr").children("td.item-createDate");
        var itemActionElement = $(element).closest("tr").children("td.item-actions").children();


        var cancelBtn = $('<button type="button" class="btn btn-default btn-sm cancel-row-btn">Cancel</button>');
        var saveBtn = $('<button type="button" class="btn btn-default btn-sm save-row-btn">Save</button>');


        //thisItemType.val(itemTypeElement.children("input.hidden").val());

        //console.log('ok' + $(element).closest("tr").children("td.item-cost").children("input.hidden"));
        itemTypeElement.children("select.hidden").removeClass("hidden").addClass("form-control").show();
        //itemTypeElement.append(thisItemType);
        itemDescElement.children("input.hidden").removeClass("hidden").addClass("form-control").show();
        itemRemarkElement.children("input.hidden").removeClass("hidden").addClass("form-control").show();
        itemCostElement.children("input.hidden").removeClass("hidden").addClass("form-control").show();
        itemCreateDateElement.children("input.hidden").removeClass("hidden").addClass("form-control").show().val(thisDate);


        $(element).closest("tr").children("td.item-type").children(".typeval").hide();
        $(element).closest("tr").children("td.item-desc").children(".descval").hide();
        $(element).closest("tr").children("td.item-remark").children(".remarkval").hide();
        $(element).closest("tr").children("td.item-cost").children(".costval").hide();
        $(element).closest("tr").children("td.item-createDate").children(".createDateVal").hide();



        //////////  CAN NOT REMOVE TWO BTN STIMULATANEOUSLY
        $(element).closest("td").find(".edit-row-btn").remove();
        $(element).closest("td").find(".delete-row-btn").remove();



        itemActionElement.parent().append(saveBtn);
        itemActionElement.parent().append(cancelBtn);




        //totalExpense();


        unBindSaveRowBtn();
        bindSaveRowBtn();

        unBindCancelBtn();
        bindCancelBtn();

    }

    function cancelItem(element)
    {
        //var TypeVal;
        var descVal = $(element).closest("tr").children("td.item-desc").find("span").text();
        var remarkVal = $(element).closest("tr").children("td.item-remark").find("span").text();
        var costVal = $(element).closest("tr").children("td.item-cost").find("span").text();
        var createDateVal = $(element).closest("tr").children("td.item-createDate").find("span").text();

        console.log(descVal);
        console.log(remarkVal);
        console.log(costVal);
        var itemTypeElement = $(element).closest("tr").children("td.item-type");
        var itemDescElement = $(element).closest("tr").children("td.item-desc");
        var itemRemarkElement = $(element).closest("tr").children("td.item-remark");
        var itemCostElement = $(element).closest("tr").children("td.item-cost");
        var itemCreateDateElement = $(element).closest("tr").children("td.item-createDate");

        var itemActionElement = $(element).closest("tr").children("td.item-actions").children();
        var editBtn = $('<button type="button" class="btn btn-default btn-sm edit-row-btn">Edit</button>');


        itemDescElement.children().remove();
        itemRemarkElement.children().remove();
        itemCostElement.children().remove();
        itemCreateDateElement.children().remove();


        //itemTypeElement.append(itemTypeDDList);
        //$('#test').replaceWith(itemTypeDDListHidden);
        itemTypeElement.children("select.test").removeClass("form-control").addClass("hidden");
        itemDescElement.append('<span class="descval" style="color: red;">'+descVal+'</span>' +
            '<input name="description[]" class="hidden" value="'+descVal+'">');
        itemRemarkElement.append('<span class="remarkval" style="color: red;">'+remarkVal+'</span>' +
            '<input name="remark[]" class="hidden" value="'+remarkVal+'">');
        itemCostElement.append('<span class="costval" style="color: red;">'+costVal+'</span>' +
            '<input name="cost[]" class="hidden" value="'+costVal+'">');
        itemCreateDateElement.append('<span class="createDateVal" style="color: red;">'+createDateVal+'</span>' +
            '<input name="createDate[]" readonly class="hidden" value="'+createDateVal+'">');

        $(element).closest("td").find(".save-row-btn").remove();
        $(element).closest("td").find(".cancel-row-btn").remove();


        itemActionElement.parent().append(editBtn);

        totalExpense();

        unBindEditRowBtn();
        bindEditRowBtn();


    }

    function saveItemRow(element)
    {
        var itemTypeElement = $(element).closest("tr").children("td.item-type");
        var itemDescElement = $(element).closest("tr").children("td.item-desc");
        var itemRemarkElement = $(element).closest("tr").children("td.item-remark");
        var itemCostElement = $(element).closest("tr").children("td.item-cost");
        var itemCreateDateElement = $(element).closest("tr").children("td.item-createDate");
        var itemActionElement = $(element).closest("tr").children("td.item-actions").children();
        var editBtn = $('<button type="button" class="btn btn-default btn-sm edit-row-btn">Edit</button>');

        //var typeVal = itemTypeElement.children("input.form-control").val();

        var typeVal = itemTypeElement.children("select.form-control").val();
        var descVal = itemDescElement.children("input.form-control").val();
        var remarkVal = itemRemarkElement.children("input.form-control").val();
        var costVal = itemCostElement.children("input.form-control").val();
        var createDateVal = itemCreateDateElement.children("input.form-control").val();

        itemDescElement.children().remove();
        itemRemarkElement.children().remove();
        itemCostElement.children().remove();
        itemCreateDateElement.children().remove();


        //itemTypeElement.append(itemTypeDDList);
        //$('#test').replaceWith(itemTypeDDListHidden);
        itemTypeElement.children("select.test").removeClass("form-control").addClass("hidden");
        itemDescElement.append('<span class="descval" style="color: red;">'+descVal+'</span>' +
            '<input name="description[]" class="hidden" value="'+descVal+'">');
        itemRemarkElement.append('<span class="remarkval" style="color: red;">'+remarkVal+'</span>' +
            '<input name="remark[]" class="hidden" value="'+remarkVal+'">');
        itemCostElement.append('<span class="costval" style="color: red;">'+costVal+'</span>' +
            '<input name="cost[]" class="hidden" value="'+costVal+'">');
        itemCreateDateElement.append('<span class="createDateVal" style="color: red;">'+createDateVal+'</span>' +
            '<input name="createDate[]" class="hidden" value="'+createDateVal+'" readonly>');



        $(element).closest("td").find(".cancel-row-btn").remove();
        $(element).closest("td").find(".save-row-btn").remove();

        itemActionElement.parent().append(editBtn);

        totalExpense();

        unBindEditRowBtn();
        bindEditRowBtn();
    }

    function addNewRow(){


        var newRow = renderNewRow();

        console.log(newRow);
        var tableLastTr = $('#expenseTable > tbody:last-child');
        tableLastTr.append(newRow);
        //$('#replaceItem2').replaceWith(itemTypeDDList);

    }

    function deleteRow(element){


        $.confirm({
            title: 'Delete',
            content: 'Do you want to delete this line item ?',
            confirmButton: 'Yes',
            confirm: function(){
                var rowLength = $('#expenseItem').length;
                $(element).closest('tr').remove();

                recountRows();
            },
            cancel: function()
            {

            }
        });
    }

    function recountRows(){
        $('#expenseTable tr').each(function (i) {
            $(this).find('td:first').text(i);
        });
        totalExpense();
    }

    function renderNewRow(){
        var rowCount = $('#expenseTable tr').length;
        var thisDate = $('#thisDate').val();
        var newRow = '<tr><td>'+ rowCount +'</td>\n\
                <td class="item-type">\
                '+ itemTypeDDList +'</td>\n\
                <td class="item-desc"><input class="form-control" id="description" name="description_input" type="text"></td>\n\
                <td class="item-remark"><input class="form-control" id="remark" name="remark_input" type="text"></td>\n\
                <td class="item-cost"><input class="form-control" id="cost" name="cost_input" type="text"></td>\n\
                <td class="item-createDate"><input class="form-control" id="createDate" name="createDate_input" type="text" value="'+thisDate+'" readonly></td>\n\
                <td class="item-actions"><button type="button" class="btn btn-default btn-sm add-row-btn">Add</button></td>\n\
                </tr>';

        return newRow;
    }

    function numberOnly()
    {

        $(".item-cost").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
//                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

    }

    function bindCancelBtn()
    {
        $('.cancel-row-btn').bind('click.cancelRow',function(){
           cancelItem($(this));
        });
    }
    function unBindCancelBtn()
    {
        $('.cancel-row-btn').unbind('click.cancelRow');
    }

    function bindDeleteBtn(){
        $('.delete-row-btn').bind('click.deleteRow',function(){
            deleteRow($(this));
        });
    }

    function unBindDeleteBtn(){
        $(".delete-row-btn").unbind('click.deleteRow');
    }

    function bindAddToRowBtn(){
        $(".add-row-btn").bind('click.addToRow', function(){
            addItemInRow($(this));
        });
    }

    function unBindAddToRowBtn(){
        $(".add-row-btn").unbind('click.addToRow');
    }

    function bindEditRowBtn(){
        $(".edit-row-btn").bind('click.editRow', function(){
            editItemInRow($(this));
        });
    }

    function unBindEditRowBtn(){
        $(".edit-row-btn").unbind('click.editRow');
    }
    function bindSaveRowBtn(){
        $(".save-row-btn").bind('click.saveRow', function(){
            saveItemRow($(this));
        });
    }

    function unBindSaveRowBtn(){
        $(".save-row-btn").unbind('click.editRow');
    }



});