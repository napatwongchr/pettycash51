
jQuery(document).ready(function($) {

    //pickadate
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();

    var $inputStartDate = $('#startDateShow').pickadate({
        format: 'dd/mm/yyyy',
        formatSubmit: 'yyyy-mm-dd'
    });
    var pickerStartDate = $inputStartDate.pickadate('picker');

    var $inputEndDate = $('#endDateShow').pickadate({
        format: 'dd/mm/yyyy',
        formatSubmit: 'yyyy-mm-dd'
    });
    var pickerEndDate = $inputEndDate.pickadate('picker');

    if(startDate != "")
    {
        pickerStartDate.set('select', startDate ,{format: 'yyyy-mm-dd'});
        pickerEndDate.set('min',new Date($('#startDate').val()));
    }
    if(endDate != "")
    {
        pickerEndDate.set('select', endDate ,{format: 'yyyy-mm-dd'});
        pickerStartDate.set('max',new Date($('#endDate').val()));
    }

    $('#startDateShow').on(
        {
            change: function()
            {
                $('#startDate').val(pickerStartDate.get('select','yyyy-mm-dd'));
                pickerEndDate.set('min',new Date($('#startDate').val()));
            }
        });
    $('#endDateShow').on(
        {
            change: function()
            {
                $('#endDate').val(pickerEndDate.get('select','yyyy-mm-dd 23:59:59'));
                pickerStartDate.set('max',new Date($('#endDate').val()));
            }
        });
    //end pickadate



    var url = "financial/query";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });


    $("#ajaxSubmit").on("click",function()
    {
        var jsonSubmitData = {"startDate" : $('#startDate').val(),
            "endDate" :   $('#endDate').val()};
        console.log('test');
        $.ajax({
            type: 'POST',
            url: url,
            data: jsonSubmitData,
            dataType: 'json'
        })
            .done(function(data)
            {
                console.log('ok');
                //console.log(data.result);
                console.log(data.result.length);
                //var responseData = data;
                $('#output_startDate').val(data.startDate);
                $('#output_endDate').val(data.endDate);
                tableCreate(data.result,data.result.length);

            })
            .error(function(data){
                console.log(data);

            });
    });


    function statusSelect(payStatus)
    {
        if(payStatus == 'pending')
        {
            var statusSelector = '<select class="form-control" name="status[]">' +
                '<option selected value="pending">pending</option>' +
                '<option value="success">success</option>' +
                '</select>';
        }
        else if(payStatus == 'success')
        {
            var statusSelector = '<select class="form-control" name="status[]">' +
                '<option value="pending">pending</option>' +
                '<option selected value="success">success</option>' +
                '</select>';
        }
        return statusSelector;

    }

    function tableCreate(expenses,length)
    {
        var table = '        <table class="table table-striped">' +
            '<thead>' +
            '<th>id</th>'+
            '<th>name</th> ' +
            '<th>วันจ่ายตัง</th> ' +
            '<th>Approve Date</th> ' +
            '<th>description</th> ' +
            '<th>status</th> ' +
            '<th> cost</th> ' +
            '<th> วันใส่ข้อมูล</th> ' +
            '</thead>' +
            '<tbody id="itemTable"></tbody>' +
            '</table>';
        $('#queryTable').find('table').remove();
        $('#queryTable').append(table);

        //append new data row to table

        console.log('wow');
        console.log(expenses);

        //var statusSelect = '<select class="form-control">' +
        //    '<option value="pending">pending</option>' +
        //    '<option value="success">success</option>' +
        //    '</select>'

        for(i=0;i<length;i++)
        {
            var selectedStatus = statusSelect(expenses[i].status);

            var lineItem = '<tr>' +
                '<td>' +
                ''+expenses[i].id+' ' +
                '<input class="hidden" name="expenseID[]" value="'+expenses[i].id+'">' +
                '</td>' +
                '<td>'+expenses[i].userName+'</td>' +
                '<td>'+expenses[i].buyDate+'</td>' +
                '<td>'+expenses[i].approveDate+'</td>' +
                '<td>'+expenses[i].description+'</td>' +
                '<td>'+expenses[i].status+' '+selectedStatus+' </td>' +
                '<td>'+expenses[i].cost+'</td>' +
                '<td>'+expenses[i].createDate+'</td>' +
                '</tr>';


            $('#itemTable').append(lineItem);
            console.log(expenses[0]);
        }
    }



    function addRow()
    {

    }


});