<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_id')->unsigned();
            $table->string('description');
            $table->string('remark')->nullable();
            $table->string('status');
            $table->date('approveDate')->nullable();
            $table->date('createDate');
            $table->decimal('cost',15,4);
            $table->integer('itemType_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('expense_id')->references('id')->on('expenses')->onDelete('cascade');
            $table->foreign('itemType_id')->references('id')->on('item_types')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expense_items');
    }
}
