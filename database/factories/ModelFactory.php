<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => 'napat',
        'email' => 'knotsite@gmail.com',
        'password' => 'xJ3a8k8f',
        'type' => 'admin',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\ItemType::class, function ($faker) {
    return [
        'name' => $faker->name,
    ];
});
