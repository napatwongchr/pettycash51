<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $toTruncate = ['users'];

    public function run()
    {
        Model::unguard();

        foreach ($this->toTruncate as $table){
            DB::table($table)->truncate();
        }

        $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}
