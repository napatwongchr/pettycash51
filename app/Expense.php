<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['buyDate','approveDate','status','totalCost'];

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function expenseItem() {
        return $this->hasMany('App\ExpenseItem');
    }
}
