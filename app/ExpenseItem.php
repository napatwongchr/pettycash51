<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseItem extends Model
{
    protected $fillable = ['description','remark','cost','itemType_id','status','approveDate','createDate'];

    public function expense(){
        return $this->belongsTo('App\Expense');
    }
    public function itemtype(){
        return $this->hasOne('App\ItemType');
    }
}
