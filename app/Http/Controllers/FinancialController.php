<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExpenseItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FinancialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('financial.index');
    }

    public function queryByDate(Request $request)
    {
        $data = $request->all();

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $expense = DB::table('expenses')
            ->leftJoin('expense_items','expenses.id','=','expense_items.expense_id')
            ->join('item_types','expense_items.itemType_id','=','item_types.id')
            ->join('users','users.id','=','expenses.user_id')
            ->whereBetween('expense_items.createDate',[$startDate,$endDate])
            ->select('expense_items.id as id','users.name as userName','buyDate','createDate','description','status','cost','approveDate')
            ->get();


        if($request->ajax())
        {
//			$members = $data['memberDetails'];

//            $questions = $request->get('questionResponses');
//			$questions = $data['questionResponses'];


            return response()->json(['result' => $expense]);
        }
    }

    public function storeStatus(Request $request)
    {
//        dd($request);
        $arrayID = $request->get('expenseID');
        $arrayStatus = $request->get('status');
        $approveDate = date('Y-m-d');

        // if status == success
        //  add approve date
        $i = 0;
        foreach($arrayID as $id)
        {
//            $thisExpense = ExpenseItem::findOrFail($id);
//            $thisExpense->update(array(
//                'approveDate' =>    $approveDate,
//                'status'  =>    'pending',
//            ));
//
            if($arrayStatus[$i] == 'success')
            {
                $thisExpense = ExpenseItem::findOrFail($id);

                $thisExpense->update(array(
                    'approveDate' =>    $approveDate,
                    'status'  =>    'success',
                ));
            }
            $i++;
        }

        return view('home');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        if($request->ajax())
        {
//			$members = $data['memberDetails'];

//            $questions = $request->get('questionResponses');
//			$questions = $data['questionResponses'];


//            echo '{"return" : "someThing"}';
            return response()->json(['startDate' => $startDate,
                                    'endDate'   =>  $endDate]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
