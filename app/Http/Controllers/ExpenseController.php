<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Expense;
use App\User;
use App\ExpenseItem;
use App\ItemType;
use Auth;
use DB;

class ExpenseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
//
//        $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
//
//        $this->middleware('subscribed', ['except' => ['fooAction', 'barAction']]);
//        $this->middleware('userType:admins');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $expenses = Expense::all();
//        dd($expenses);
        $users = DB::table('users');

//        $user = $users->where('id','2')->value('name');

//        dd($user);
        return view('expense.index',compact('expenses','users'));
    }

    public function userExpense()
    {
        $userID = Auth::user()->id;

        $userName = Auth::user()->name;
        $expenseItems = DB::table('expenses')
                        ->leftJoin('expense_items','expenses.id','=','expense_items.expense_id')
                        ->join('item_types','expense_items.itemType_id','=','item_types.id')
                        ->where('user_id','=',$userID)
                        ->select('expense_items.id as itemID','buyDate','approveDate','description','status','cost','createDate','expenses.id as expenseID')
                        ->get();

//        dd($expenseItems);
        return view('expense.userExpense',compact('expenseItems','userName'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $expense = null;
        $arrayOfLineItem = [];
        $arrayOfItemType = ItemType::all();
        return view('expense.create',compact('expense','arrayOfLineItem','arrayOfItemType'));
    }
    
    public function shareme(){
        
        return view('shareme');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $boughtDate = $request->get('boughtDate');
        $totalCost = $request->get('totalCost');

        $itemTypeArray = $request->get('itemType');
        $descriptionArray = $request->get('description');
        $remarkArray = $request->get('remark');
        $costArray = $request->get('cost');

//        dd(\Auth::user());

        $expense = \Auth::user()->expenses()->create(array(
            'buyDate' =>    $boughtDate,
            'totalCost' =>  $totalCost,
        ));

//        dd($expense);
//        $thisExpense = Expense::latest('id')->first();
        $createDate = date('Y-m-d');
        $i = 0;
        foreach($costArray as $cost)
        {
            $itemType = $itemTypeArray[$i];
            $description = $descriptionArray[$i];
            $remark = $remarkArray[$i];

            $lineItem = new ExpenseItem(array(
            'itemType_id'  =>  $itemType,
            'description'   => $description,
            'status'    =>  'pending',
            'remark'    =>  $remark,
            'cost'      =>  $cost,
            'createDate'    =>  $createDate

            ));

            $expense->expenseItem()->save($lineItem);
            $i++;
        }
        return redirect('expense');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $expense = Expense::findOrFail($id);
        $arrayOfLineItem = $expense->expenseItem()->get()->all();
        $arrayOfItemType = ItemType::all();
        return view('expense.edit',compact('expense','arrayOfLineItem','arrayOfItemType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
//        dd($request);
        $boughtDate = $request->get('boughtDate');
        $totalCost = $request->get('totalCost');

        $itemTypeArray = $request->get('itemType');
        $descriptionArray = $request->get('description');
        $remarkArray = $request->get('remark');
        $costArray = $request->get('cost');

        $thisExpense = Expense::findOrFail($id);

//        dd($thisExpense);
        $thisExpense->update(array(
            'buyDate' =>    $boughtDate,
            'totalCost' =>  $totalCost,
        ));

        ExpenseItem::where('expense_id','=',$id)->delete();

        $createDate = date('Y-m-d');
        $i = 0;
        foreach($costArray as $cost)
        {
            $itemType = $itemTypeArray[$i];
            $description = $descriptionArray[$i];
            $remark = $remarkArray[$i];

            $lineItem = new ExpenseItem(array(
                'itemType_id'  =>  $itemType,
                'description'   => $description,
                'status'  =>    'pending',
                'createDate'    =>  $createDate,
                'remark'    =>  $remark,
                'cost'      =>  $cost

            ));
            $thisExpense->expenseItem()->save($lineItem);
            $i++;


        }
        return redirect('expense');



    }


//    delete each item in table
    public function delete(Request $request)
    {
        $id = $request->get('itemID');
        $thisExpenseItem = ExpenseItem::findorFail($id);
        $thisExpenseItem->delete();
        return redirect('userExpense');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $thisExpense = Expense::findOrFail($id);
        $thisExpense->delete();
        return redirect('expense');
    }
}
