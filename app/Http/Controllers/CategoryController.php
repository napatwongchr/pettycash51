<?php

namespace App\Http\Controllers;

use App\ItemType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
//
//        $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
//
//        $this->middleware('subscribed', ['except' => ['fooAction', 'barAction']]);
        $this->middleware('userType:admin',['except' =>  ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $arrayOfItemType = ItemType::all();

        return view('category.index',compact('arrayOfItemType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $itemType = null;
        return view('category.create',compact('itemType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $itemTypeName = $request->get('name');


        $itemType = ItemType::create(array(
            'name' =>    $itemTypeName,
        ));
        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $itemType = ItemType::findOrFail($id);
        return view('category.edit',compact('itemType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
//        dd('ok');
        $itemTypeName = $request->get('name');
        $thisItemType = ItemType::findOrFail($id);
        $thisItemType->update(array(
            'name' =>    $itemTypeName
        ));
        return redirect('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
