<?php

use App\User;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');

Route::get('shareme','ExpenseController@shareme');

Route::get('login', function(){
    User::truncate();

    $user = User::create([
        'name' => 'John',
        'email' => 'example@ex.com',
        'password' => bcrypt('password'),
        'type'  =>  'admissn',
    ]);

    Auth::login($user);

    return redirect('/');
});

Route::resource('test','testMidController');

Route::get('userExpense','ExpenseController@userExpense');
Route::post('userExpense','ExpenseController@delete');

Route::resource('expense','ExpenseController');

Route::resource('category','CategoryController');

Route::POST('financial/query','FinancialController@queryByDate');
Route::POST('financial/status','FinancialController@storeStatus');
Route::resource('financial','FinancialController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
