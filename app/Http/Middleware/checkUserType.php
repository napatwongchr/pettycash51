<?php

namespace App\Http\Middleware;

use Closure;

class checkUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        $user = $request->user();

        if($user && $user->checkType($type))
        {
            return $next($request);
        }

        return view('home');

    }
}
