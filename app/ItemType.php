<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $fillable = ['name'];


    public function expenseitem() {
        return $this->belongsToMany('App\ExpenseItem');
    }
}
